#!/usr/bin/env bash

echo "Create a new pipeline 'ffx-ci-demo' on concourse.ci"
fly -t ci set-pipeline --pipeline ffx-ci-demo --config ./ci/pipeline.yml --load-vars-from ~/.ssh/credentials.yml