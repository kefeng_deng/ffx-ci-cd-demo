#!/usr/bin/env bash

set -o -e -x errexit

ls -Alu

git clone ./ffx-ci-demo ./ffx-ci-demo-modified

cd ./ffx-ci-demo-modified

echo "start to verify and build the application"
./gradlew -q --console plain clean resolveAllDependencies build

cd ..