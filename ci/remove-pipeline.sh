#!/usr/bin/env bash

echo "Remove 'ffx-ci-demo' pipeline from concourse.ci"
fly -t ci destroy-pipeline --pipeline ffx-ci-demo