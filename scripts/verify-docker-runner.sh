#!/usr/bin/env bash


# Function check whether docker container is running or not
function checkContainerRunningStatus() {
    docker inspect --format "{{ .State.Status }}" $1
}

# Function check whether there are some INFO log messages
function checkExistingApplicationLogs() {
    docker logs $1 > /dev/null 2>&1 | grep 'INFO\|WARN' | wc -l
}

# Function check whether application is ready
function isApplicationStarted() {
    docker logs ${DOCKER_ID} | grep -RL "Started Application in" | wc -l
}

# Function to perform health check of application within container
function checkApplicationHealth() {
    curl -X GET --fail http://localhost:8080/healthcheck/light
}

IMAGE_ID=`docker images -q ffxnz-nlp-service`

echo "Start a new docker container with image ID $IMAGE_ID"
docker run -d -p 8080:8080 --name ffx-nlp-service-docker-verify $IMAGE_ID

CONTAINER_ID=`docker ps -aqf "name=ffx-nlp-service-docker-verify"`
echo "Container ID is $CONTAINER_ID"

# Wait 4 second to start a new container
echo "Checking container status ..."
sleep 4
CONTAINER_STATUS=$(checkContainerRunningStatus "$CONTAINER_ID")
APPLICATION_LOG_SIZE=$(checkExistingApplicationLogs "$CONTAINER_ID")

echo "Current container status is [$CONTAINER_STATUS]"
if [ "$CONTAINER_STATUS" == "exited" ]; then
    echo "Failed to start a new container"
    exit 1
fi
echo "Started container successfully"

# Wait 3 seconds for application be started up
echo "Check application startup ..."
sleep 3
if [ ${APPLICATION_LOG_SIZE} -eq 0 ]; then
    echo "Failed to launch application"
    exit 1
fi
echo "Application is starting ..."

# Check the application status, and set timeout for 30 seconds
COUNTER=0
APPLICATION_STATUS=""
while [ ${COUNTER} -lt 10 ] || [ "$APPLICATION_STATUS" == "" ]; do

    echo "Waiting for application to be ready ... $COUNTER"
    if [ isApplicationStarted -eq 1 ]; then
        APPLICATION_STATUS="OK"
        break
    fi

    # Increase counter until 30 seconds as timeout
    let COUNTER=COUNTER+1

    sleep 3

done

if [ "$APPLICATION_STATUS" == "" ]; then
    echo "Failed to launch application"
    exit 1
fi

echo "Application be launched successfully"

echo "Checking application health ..."
if [ "$(checkApplicationHealth)" == "OK" ]; then
    echo "Can not reach application in container"
    exit 1
fi

echo "Verify application container successfully"

echo "Deleting docker ..."
docker rm -f "$CONTAINER_ID"
exit 0
