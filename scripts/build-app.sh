#!/bin/bash

echo "=============================================="
echo " >>> Building application within a docker container ..."

PWD=$(pwd)

docker run -it --rm -v $PWD:/app -w /app openjdk:8 sh -c "./gradlew -q --console plain clean resolveAllDependencies build"
