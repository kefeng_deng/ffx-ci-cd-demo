#!/usr/bin/env bash

set -o -e -x errexit

ls -Alu

cd ./ffx-ci-demo-modified

ls -Alu

PROJECT_FILE=$(basename `find ./target/lib -type f -name '*.jar'`)
PWD=$(pwd)

echo ">>> Copying Dockerfile ..."
cp -vf $PWD/ci/docker/Dockerfile $PWD/target/lib/Dockerfile

echo ">>> Start to build.sh docker image ..."
docker build -t ffxnz-nlp-service --build-arg PROJECT_JAR=$PROJECT_FILE -f $PWD/target/lib/Dockerfile $PWD/target/lib/.