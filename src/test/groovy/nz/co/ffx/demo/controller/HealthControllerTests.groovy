// Copyright Fairfax Media 2017
package nz.co.ffx.demo.controller

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner)
@WebMvcTest(HealthController)
class HealthControllerTests {

    @Autowired
    MockMvc mockMvc

    @Test
    void shouldReturnOK() {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/healthcheck/light")
            .accept(MediaType.TEXT_PLAIN))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string('OK'))



    }


}
