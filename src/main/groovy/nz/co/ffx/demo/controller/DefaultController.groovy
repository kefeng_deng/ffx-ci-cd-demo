// Copyright Fairfax Media 2017
package nz.co.ffx.demo.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Default Controller for the index page
 */
@Controller
class DefaultController {
    
    @RequestMapping(path='', method = RequestMethod.GET)
    @ResponseBody
    String indexHandler(HttpServletRequest request, HttpServletResponse response) {
        return 'Welcome to FFX CI/CI practice demo'
    }
}
