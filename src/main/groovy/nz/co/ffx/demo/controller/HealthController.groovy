package nz.co.ffx.demo.controller

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

// Copyright Fairfax Media 2017
@RestController
@RequestMapping(path = '/healthcheck', method = RequestMethod.GET)
class HealthController {

    @RequestMapping(path = '/light', produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    String lightHealthCheck() {
        return HttpStatus.OK.reasonPhrase
    }


}
