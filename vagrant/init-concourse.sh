#!/usr/bin/env bash

echo "Install packages to allow apt to use a repository over HTTPS"
apt-get install -y apt-transport-https ca-certificates curl software-properties-common

echo "Add docker's official GPG key"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "Set up the stable repository"
sudo add-apt-repository -y \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

echo "Update and install docker"
sudo apt-get -y update && sudo apt-get install -y docker-ce