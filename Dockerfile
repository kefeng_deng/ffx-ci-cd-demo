FROM openjdk:8-jre-alpine

LABEL maintainer Kefeng Deng (kefeng.deng@fairfaxmedia.co.nz)
LABEL description 'FFX CI/CD Demo'

ENV PROJECT_HOME /app

RUN mkdir -p $PROJECT_HOME

WORKDIR $PROJECT_HOME

# Install the new entry-point script
COPY ./docker/entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

# Install application JAR
ADD ./build/libs/ffx-demo-*.jar ./app.jar

# Enable Health Check
HEALTHCHECK --interval=30s --timeout=30s --retries=3 CMD curl --fail http://localhost:8080/healthcheck/light || exit 1

EXPOSE 8080
ENV JAVA_OPTS " "

# Set the entry-point script
ENTRYPOINT ["/entrypoint.sh"]

# Run application
CMD ["sh", "-c", "java $JAVA_OPTS -Dfile.encoding=UTF-8 -jar app.jar"]